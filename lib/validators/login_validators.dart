mixin login_validators {
  String? validateEmail(String? email) {
    if (!email!.contains('@')) {
      return 'Email is invalid !';
    }
    return null;
  }

  String? validatePassword(String? password) {
    if (password!.length < 6) {
      return 'Password must be at least 6 letters !';
    }
    return null;
  }
}