import 'package:flutter/material.dart';
import 'login_screen.dart';
import 'stopwatch_screen.dart';

class MyApp extends StatelessWidget {
  const MyApp({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.deepPurple,
        backgroundColor: Colors.deepPurpleAccent,
      ),
      debugShowCheckedModeBanner: false,
      routes: {
        LoginScreen.route: (context) => LoginScreen(),
        StopwatchScreen.route: (context) => StopwatchScreen(),
      },
      initialRoute: '/',
    );
  }
}