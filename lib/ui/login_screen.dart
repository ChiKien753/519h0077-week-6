import 'package:flutter/material.dart';
import 'package:stopwatch_519h0077/ui/stopwatch_screen.dart';
import '../validators/login_validators.dart';

class LoginScreen extends StatefulWidget {
  static const route= '/';
  const LoginScreen({ Key? key }) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with login_validators{
  final formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Login screen'),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: buildLoginForm(),
      ),
    );
  }

  // Build Login Form
  Widget buildLoginForm() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          emailField(),
          passwordField(),
          loginBtn(),
        ],
      ),
    );
  }

  // Build Email Field
  Widget emailField() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: TextFormField(
        controller: emailController,
        decoration: InputDecoration(
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.deepPurpleAccent, width: 2.0),
            borderRadius: BorderRadius.circular(25.0),
          ),
          icon: Icon(Icons.person),
          labelText: 'Email',
        ),
        validator: validateEmail,
      ),
    );
  }

  // Build Password Field
  Widget passwordField() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: TextFormField(
        controller: passwordController,
        obscureText: true,
        decoration: InputDecoration(
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.deepPurpleAccent, width: 2.0),
            borderRadius: BorderRadius.circular(25.0),
          ),
          icon: const Icon(Icons.lock),
          labelText: 'Password',
        ),
        validator: validatePassword,
      ),
    );
  }

  // Build Login button
  Widget loginBtn() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: ElevatedButton(
        onPressed: validate, 
        child: const Text('Login'),
        style: ElevatedButton.styleFrom(primary: Colors.deepPurple),
      ),
    );
  }

  void validate() {
    final form = formKey.currentState;

    if(!form!.validate()) {
      return;
    }
    else {
      final email = emailController.text;
      final password = passwordController.text;

      Navigator.of(context).pushReplacementNamed(StopwatchScreen.route, arguments: email);

      setState(() {
        
      });
    }
  }
}
